package no.ntnu.idatx2003.oblig3.cardgame;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class HandOfCards {
  private final List<PlayingCard> hand;

  public HandOfCards(){
    hand = new ArrayList<>();
  }

  public boolean checkIfFlush() {
    return IntStream.range(0, hand.size() - 4)
        .anyMatch(i -> hand.subList(i, i + 5)
            .stream()
            .allMatch(card -> card.getSuit() == hand.get(i).getSuit()));
  }


  /*
  public int checkNumberOfHeart(){
    int numberOfHearts = 0;
    for (int i = 0; i < hand.size(); i++) {
      if (getHand().get(i).getSuit() == 'H') {
        numberOfHearts++;
      }
    }
    return numberOfHearts;
  }

   */

  public int checkNumberOfHeart() {
    return (int) hand.stream()
        .filter(card -> card.getSuit() == 'H')
        .count();
  }


  /*
  public boolean checkIfContainsQueenSpades(){
    boolean containsQueenOfSpades = false;
    for (int i = 0; i < hand.size(); i++) {
      if (getHand().get(i).getFace() == 12 && getHand().get(i).getSuit() == 'S') {
        containsQueenOfSpades = true;
      }
    }
    return containsQueenOfSpades;
  }

   */

  public boolean checkIfContainsQueenSpades() {
    return hand.stream()
        .anyMatch(card -> card.getFace() == 12 && card.getSuit() == 'S');
  }

  /*
  public int checkSum(){
    int sum = 0;
    for (int i = 0; i < hand.size(); i++) {
      sum += getHand().get(i).getFace();
    }
    return sum;
  }

   */

  public int checkSum() {
    return hand.stream()
        .mapToInt(PlayingCard::getFace)
        .sum();
  }
  public List<PlayingCard> getHand() {
    return hand;
  }

  public void addCard(PlayingCard playingCard) {
    hand.add(playingCard);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();

    hand.forEach(card -> sb.append(card.getAsString()).append("                 "));

    return sb.toString();
  }
}
