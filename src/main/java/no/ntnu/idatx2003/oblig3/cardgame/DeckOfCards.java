package no.ntnu.idatx2003.oblig3.cardgame;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DeckOfCards {

  private final char[] suits = {'S', 'H', 'D', 'C'};
  private final List<PlayingCard> deck;
  Random random = new Random();

  public DeckOfCards() {
    deck = new ArrayList<>();
    for (int i = 0; i < 52; i++) {
      char s = suits[i / 13];
      int f = (i % 13) + 1;
      deck.add(new PlayingCard(s, f));
    }
  }


  public HandOfCards dealHand(int n) {
    if (n < 0 || n > deck.size()) {
      throw new IllegalArgumentException("Invalid number of cards to deal");
    }
    HandOfCards hand = new HandOfCards();
    for (int i = 0; i < n; i++) {
      int cardIndex = random.nextInt(deck.size());
      if (hand.getHand().contains(deck.get(cardIndex))) {
        i--;
      } else {
        hand.addCard(deck.get(cardIndex));
      }
    }
    return hand;
  }

  public int size() {
    return deck.size();
  }

  public String CardsToString() {
    StringBuilder string = new StringBuilder();
    for (PlayingCard card : deck) {
      string.append(card.getAsString()).append("\n");
    }
    return string.toString();
  }

  public List<PlayingCard> getDeck() {
    return deck;
  }
}



