package no.ntnu.idatx2003.oblig3.cardgame;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class DeckOfCardsTest {

  @Test
  void testDeckOfCards() {
    DeckOfCards deck = new DeckOfCards();
    assertEquals(52, deck.getDeck().size());
  }

  @Test
  void testDealHand() {
    DeckOfCards deck = new DeckOfCards();
    HandOfCards hand = deck.dealHand(5);
    assertEquals(5, hand.getHand().size());
  }

  @Test
  void testDealHandInvalid() {
    DeckOfCards deck = new DeckOfCards();
    assertThrows(IllegalArgumentException.class, () -> deck.dealHand(53));
  }

}