package no.ntnu.idatx2003.oblig3.cardgame;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;

class HandOfCardsTest {
  @Test
  void checkIfFlush() {
    HandOfCards hand = new HandOfCards();
    hand.getHand().add(new PlayingCard('H', 1));
    hand.getHand().add(new PlayingCard('H', 2));
    hand.getHand().add(new PlayingCard('H', 3));
    hand.getHand().add(new PlayingCard('H', 4));
    hand.getHand().add(new PlayingCard('H', 5));
    assertTrue(hand.checkIfFlush());
  }
  @Test
  void checkIfNotFlush() {
    HandOfCards hand = new HandOfCards();
    hand.getHand().add(new PlayingCard('S', 1));
    hand.getHand().add(new PlayingCard('H', 2));
    hand.getHand().add(new PlayingCard('H', 3));
    hand.getHand().add(new PlayingCard('H', 4));
    hand.getHand().add(new PlayingCard('H', 5));
    assertFalse(hand.checkIfFlush());
  }

  @Test
  void checkNumberOfHeart() {
    HandOfCards hand = new HandOfCards();
    hand.getHand().add(new PlayingCard('H', 1));
    hand.getHand().add(new PlayingCard('S', 2));
    hand.getHand().add(new PlayingCard('H', 3));
    hand.getHand().add(new PlayingCard('C', 4));
    hand.getHand().add(new PlayingCard('H', 5));
    assertEquals(3, hand.checkNumberOfHeart());
  }

  @Test
  void checkIfContainsQueenSpades() {
    HandOfCards hand = new HandOfCards();
    hand.getHand().add(new PlayingCard('H', 1));
    hand.getHand().add(new PlayingCard('S', 2));
    hand.getHand().add(new PlayingCard('H', 3));
    hand.getHand().add(new PlayingCard('C', 4));
    hand.getHand().add(new PlayingCard('S', 12));
    assertTrue(hand.checkIfContainsQueenSpades());
  }

  @Test
  void checkIfNotContainsQueenSpades() {
    HandOfCards hand = new HandOfCards();
    hand.getHand().add(new PlayingCard('H', 1));
    hand.getHand().add(new PlayingCard('S', 2));
    hand.getHand().add(new PlayingCard('H', 3));
    hand.getHand().add(new PlayingCard('C', 4));
    hand.getHand().add(new PlayingCard('S', 11));
    assertFalse(hand.checkIfContainsQueenSpades());
  }

  @Test
  void checkSum() {
    HandOfCards hand = new HandOfCards();
    hand.getHand().add(new PlayingCard('H', 1));
    hand.getHand().add(new PlayingCard('S', 2));
    hand.getHand().add(new PlayingCard('H', 3));
    hand.getHand().add(new PlayingCard('C', 4));
    hand.getHand().add(new PlayingCard('S', 5));
    assertEquals(15, hand.checkSum());
  }
}